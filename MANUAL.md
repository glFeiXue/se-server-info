Big thanks to folks at **http://www.insurgency.pro** for creating the API and for all the contributions to mod community.

You can invoke script by either specifying server's IP:port or use one of aliases defined in '*sinfo.cfg*'

`php sinfo.php 192.168.0.101:27015`

`php sinfo.php servalias`

Configuration file **must** be in same folder as script. There are few (commented) options in script you might want to have a look at.
