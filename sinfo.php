<?php

// List of roles. Feel free to add
$roles=array(
			"[ADMIN]",
			"[VIP]",
			"[ENGINEER]",
			"[ENG]",
			"[MEDIC]",
			"[MG]"
	);
	
$cfg=false;
$scriptDir=dirname(__FILE__);

if(file_exists($scriptDir."/sinfo.cfg")) {
	$cfgA=readCfg($scriptDir."/sinfo.cfg");
	
	if(!$cfgA[0]) {
		echo "ERR: ".$cfgA[1]."\n";
		exit(1);
	}
	
	$cfg=$cfgA[1];
}

if($argc<2) {
	usage();
	exit(0);
}

$srvArg=$argv[1];
$srvNet=false;
$srvAls="";

if($cfg) {
	foreach($cfg as $a) {
		if(strtoupper($a["alias"])==strtoupper($srvArg)) {
			$srvNet=$a["server"];
			$srvAls=$srvArg;
			break;					
		}	
	}
	if(!$srvNet)
		$srvNet=$srvArg;	
} else {
	$srvNet=$srvArg;
}

if(!validSrvAdr($srvNet)) {
	echo "ERR: '".$srvNet."' is not a valid server address\n";
	exit(1);
}

if($srvAls) {
	echo "Querying '".$srvAls."' (".$srvNet.")\n";
} else {
	echo "Querying ".$srvNet."\n";
}

$rf=file_get_contents("http://www.insurgency.pro/api/".$srvNet);
if(!$rf) {
	echo "ERR: API returned nothing or timed out\n";
	exit(1);
}

$shell=json_decode($rf,true);

if(!isset($shell["name"])) {
	echo "ERR: API returned nothing or timed out\n";
	exit(1);
}

// If you want to poke around API response
//file_put_contents($scriptDir."/insinfo.txt",print_r($shell,true));

displayInfo($shell);
echo "\n";

// ==============================================================

function usage() {
	echo "Usage:\n";
	echo "  php sinfo.php <server | alias>\n";
}

// --------------------------------------------------------------

function readCfg($file) {
	$fin=fopen($file,"r");
	if(!$fin)
		return array(false,"Couldn't read configuration in\n  ".$file);
		
	$lC=1;
	
	while($line=fgets($fin)) {
		$line=str_replace(PHP_EOL,"",$line);
		if(substr($line,0,1)==";") {
			$lC++;
			continue;
		}
			
		$lA=explode("=",$line);
		if(count($lA)!=2) {
			fclose($fin);
			return array(false,"Bad server info '".$line."' on line ".$lc);		
		}
		
		$cfg[]=array("alias" => trim($lA[0]), "server" => trim($lA[1]));		
	}
	
	fclose($fin);
	
	return array(true,$cfg);
}

// --------------------------------------------------------------

function validSrvAdr($srvNet) {
	$sA=explode(".",$srvNet);
	if(count($sA)!=4)
		return false;
		
	$pA=explode(":",$sA[3]);
	if(count($pA)!=2)
		return false;
		
	$sA[3]=$pA[0];
	foreach($sA as $s)
		if(intval($s)>255)
			return false;
			
	return true;
}

// ---------------------------------------------------------

function displayInfo($arr) {
	global $roles;
	
	$pL=array();
	
	echo "  Host: ".$arr["name"]."\n";
	echo "   Map: ".$arr["map"]."\n";
	
	// Additional info you may want to display
	
	// IP and port (it's shown above but ...)
	//echo "  Addr: ".$arr["query"]["host"].":".$arr["query"]["port"]."\n";
	
	// Game served if you are querying other Source engine servers
	//echo "  Game: ".$arr["raw"]["game"]."\n";
	
	// Server version
	//echo "   Ver: ".$arr["raw"]["version"]."\n";
	
	// Host env.
	//echo "   Env: ".($arr["raw"]["environment"]=="l"?"Linux":"Windows")."\n";
	
	// Is host secure?
	//echo "   Sec: ".($arr["raw"]["secure"]?"Yes":"No")."\n";
	
	echo "\n";
	
	foreach($arr["players"] as $player) {
		$rt="";
		$plClean=preg_replace('/[\x00-\x1F\x7F-\xFF]/', "_",$player["name"]);
		
		for($r=0; $r<count($roles); $r++) {
			if(strpos($player["name"],$roles[$r])!==false) {
				$plClean=str_replace($roles[$r],"",$plClean);
				$rt.=$r;				
			}	else {
				$rt.="Z";					
			}
		}
		
		// Attempt to remove clan tags because ... 2020?
		$plClean=preg_replace('/<[^>]*>/','',$plClean);
		$plClean=preg_replace('/\|.*?\|/','',$plClean);
		$plClean=preg_replace('/\=.*?\=/','',$plClean);
		$plClean=preg_replace('/\(.*?\)/','',$plClean);
		$plClean=preg_replace('/\[.*?\]/','',$plClean);
		// ---------------------------------------------
		
		$pK=substr(trim($plClean),0,24);
		
		$pL[]=$rt.$pK;
		$plD[$pK]=array("time" => $player["time"], "score" => $player["score"]);	
	}
	
	// Sort by role and name
	natcasesort($pL);

	$i=0;
	foreach($pL as $p) {
		$pK=substr($p,count($roles));
		echo sprintf("%2d. %-24s ... ",$i+1,$pK);
		echo str_replace("00:","   ",gmdate("H:i ... ", $plD[$pK]["time"]));
		echo sprintf("%6s .. ",number_format($plD[$pK]["score"],0,".",","));
		
		for($r=0; $r<count($roles); $r++) {
			if(substr($p,$r,1)!="Z") {
				echo sprintf("%-12s",$roles[substr($p,$r,1)]);
			}				
		}
		echo "\n";

		$i++;			
	}		

}
?>