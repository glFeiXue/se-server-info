NOTE: This was not yet tested on Windows nor Mac. I don't do either. Never will. Theoretically it should work. Only issues I can forsee is handling of directories. If that is the case it should be only a matter of changing a line or ten.

If you know how to handle script execution, manage files and all that than stop wasting time on this file and go read MANUAL.md

1. You will need PHP Command Line Interpreter. Get it from either your repositories or directly from php.net
2. Basic installation will do, script uses no extra modules. Default php.ini should work just fine.
3. Create a folder and pull files from this repo.
4. Read MANUAL.md or just run script with no arguments; like 'php sinfo.php' to get basic syntax.
5. Optionally, see included batch script template 'sinfo.sh' and modify accordingly
